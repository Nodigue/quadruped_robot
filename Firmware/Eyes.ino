int eyes_pin = A2;

void init_eyes() {
  pinMode(eyes_pin, OUTPUT);
}

void open_eyes() {
  digitalWrite(eyes_pin, HIGH);
}

void close_eyes() {
  digitalWrite(eyes_pin, LOW);
}
