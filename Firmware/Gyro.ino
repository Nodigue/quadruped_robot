#include <Wire.h>
#include <math.h>

#define MPU_ADDR 0x68
#define PWR_MGMT_1 0x6B

#define GYRO_CONFIG 0x1B
#define ACCEL_CONFIG 0x1C

#define GYRO_xOUT 0x43
#define ACCEL_xOUT 0x3B

int16_t raw_gyro_data[3];
float gyro_data[3];

int16_t raw_accel_data[3];
float accel_data[3];

float angles[2];
float start_angles[2];

float tilts[2];

void init_mpu() {
  Wire.begin();

  Wire.beginTransmission(MPU_ADDR);
  Wire.write(PWR_MGMT_1);
  Wire.write(0b0000000);
  Wire.endTransmission();
  
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(GYRO_CONFIG);
  Wire.write(0b0000000);
  Wire.endTransmission();
  
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(ACCEL_CONFIG);
  Wire.write(0b0000000);
  Wire.endTransmission();
  
  get_start_angles();
}

void get_raw_gyro_data() {
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(GYRO_xOUT);
  Wire.endTransmission();

  Wire.requestFrom(MPU_ADDR, 6);

  raw_gyro_data[2] = Wire.read() << 8 | Wire.read();
  raw_gyro_data[1] = Wire.read() << 8 | Wire.read();
  raw_gyro_data[0] = Wire.read() << 8 | Wire.read();
}

void get_gyro_data() {
  get_raw_gyro_data();

  for (int i = 0; i < 3; i++)
    gyro_data[i] = raw_gyro_data[i] / 131.0;
}

void get_raw_accel_data() {
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(ACCEL_xOUT);
  Wire.endTransmission();

  Wire.requestFrom(MPU_ADDR, 6);

  raw_accel_data[2] = - (Wire.read() << 8 | Wire.read());
  raw_accel_data[1] = Wire.read() << 8 | Wire.read();
  raw_accel_data[0] = - (Wire.read() << 8 | Wire.read());
}

void get_accel_data() {
  get_raw_accel_data();

  for (int i = 0; i < 3; i++)
    accel_data[i] = raw_accel_data[i] / 16384.0;
}

void get_angles() {
  get_gyro_data();
  get_accel_data();

  angles[0] = 0.90 * (angles[0] + gyro_data[0] * 0.01) + 0.1 * atan2(accel_data[2], accel_data[1]) * 180 / PI;
  angles[1] = 0.90 * (angles[1] + gyro_data[1] * 0.01) + 0.1 * atan2(accel_data[2], accel_data[0]) * 180 / PI;
}

void get_start_angles() {
  unsigned long timer = millis();

  while (millis() - timer < 5000) {
    get_angles();
    delay(10);
  }

  start_angles[0] = angles[0];
  start_angles[1] = angles[1];
}

void get_tilts() {
  get_angles();

  tilts[0] = angles[1] - start_angles[1];
  tilts[1] = angles[0] - start_angles[0];
}

float get_fb_tilt() {
  get_tilts();

  return tilts[0];
}

float get_lr_tilt() {
  get_tilts();
  
  return tilts[1];
}
