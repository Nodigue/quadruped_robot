#include <Servo.h>

const float pi = 3.14159;

const unsigned BODY_HEIGHT = 950;
const unsigned BODY_WIDTH = 500;

const float l1 = 275;
const float l2 = 540;
const float l3 = 800;

Servo B_L_1, B_L_2, B_L_3;
Servo F_L_1, F_L_2, F_L_3;
Servo B_R_1, B_R_2, B_R_3;
Servo F_R_1, F_R_2, F_R_3;

Servo legs[4][3] = {{B_L_1, B_L_2, B_L_3}, {F_L_1, F_L_2, F_L_3}, {B_R_1, B_R_2, B_R_3}, {F_R_1, F_R_2, F_R_3}};
int legs_pins[4][3] = {{A0, 3, 4}, {5, 6, 7}, {8, 9, 10}, {11, 12, 13}};

int legs_angles[4][3];

int current_shoulders_positions[4][3];
int current_feet_positions[4][3];

int new_shoulders_positions[4][3];
int new_feet_positions[4][3];

float current_body_inclinaisons[3];

int get_servo_pin(int leg_nb, int servo_nb) {
  return legs_pins[leg_nb][servo_nb];
}

void attach_servos() {
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 3; j++) {
      legs[i][j].attach(legs_pins[i][j]);
    }
  }
}

void detach_servos() {
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 3; j++) {
      legs[i][j].detach();
    }
  }
}

void set_angle_servo(int leg_nb, int servo_nb, int angle) {
  if ((leg_nb >= 0 && leg_nb <= 3) && (servo_nb >= 0 && servo_nb <= 2)) {
    if (angle > 10 && angle < 170) {

      legs_angles[leg_nb][servo_nb] = angle;
      legs[leg_nb][servo_nb].write(angle);
    }
  }
}

void calculate_shoulders_positions(float roll, float pitch, float yaw) {

  //Conversion en radian
  roll *= (pi / 180);
  pitch *= (pi / 180);
  yaw *= (pi / 180);

  //Calcul de la différence
  float droll = roll - current_body_inclinaisons[0];
  float dpitch = pitch - current_body_inclinaisons[1];
  float dyaw = yaw - current_body_inclinaisons[2];

  Serial.println(droll);

  //ROLL
  if (droll != 0) {
    
    if (droll > 0) {
     
      new_shoulders_positions[0][0] = current_shoulders_positions[0][0] - (BODY_WIDTH / 2) * cos(droll);
      new_shoulders_positions[1][0] = current_shoulders_positions[1][0] + (BODY_WIDTH / 2) * cos(droll);
      new_shoulders_positions[2][0] = current_shoulders_positions[2][0] - (BODY_WIDTH / 2) * cos(droll);  
      new_shoulders_positions[3][0] = current_shoulders_positions[3][0] + (BODY_WIDTH / 2) * cos(droll);
   
    }else {
  
      new_shoulders_positions[0][0] = current_shoulders_positions[0][0] + (BODY_WIDTH / 2) * cos(droll);
      new_shoulders_positions[1][0] = current_shoulders_positions[1][0] - (BODY_WIDTH / 2) * cos(droll);
      new_shoulders_positions[2][0] = current_shoulders_positions[2][0] + (BODY_WIDTH / 2) * cos(droll);  
      new_shoulders_positions[3][0] = current_shoulders_positions[3][0] - (BODY_WIDTH / 2) * cos(droll);
      
    }
  
    new_shoulders_positions[0][1] = current_shoulders_positions[0][1];
    new_shoulders_positions[1][1] = current_shoulders_positions[1][1];
    new_shoulders_positions[2][1] = current_shoulders_positions[2][1];
    new_shoulders_positions[3][1] = current_shoulders_positions[3][1];
      
    new_shoulders_positions[0][2] = current_shoulders_positions[0][2] - (BODY_WIDTH / 2) * sin(droll);
    new_shoulders_positions[1][2] = current_shoulders_positions[1][2] - (BODY_WIDTH / 2) * sin(droll);
    new_shoulders_positions[2][2] = current_shoulders_positions[2][2] + (BODY_WIDTH / 2) * sin(droll);
    new_shoulders_positions[3][2] = current_shoulders_positions[3][2] + (BODY_WIDTH / 2) * sin(droll);
  
  }
  /*
  //PITCH
  new_shoulders_positions[0][0] = current_shoulders_positions[0][0];
  new_shoulders_positions[1][0] = current_shoulders_positions[1][0];
  new_shoulders_positions[2][0] = current_shoulders_positions[2][0];  
  new_shoulders_positions[3][0] = current_shoulders_positions[3][0];

  if (dpitch > 0) {
    
    new_shoulders_positions[0][1] = current_shoulders_positions[0][1] + (BODY_HEIGHT / 2) * cos(dpitch);
    new_shoulders_positions[1][1] = current_shoulders_positions[1][1] - (BODY_HEIGHT / 2) * cos(dpitch);
    new_shoulders_positions[2][1] = current_shoulders_positions[2][1] + (BODY_HEIGHT / 2) * cos(dpitch);
    new_shoulders_positions[3][1] = current_shoulders_positions[3][1] - (BODY_HEIGHT / 2) * cos(dpitch);
    
  }else {

    new_shoulders_positions[0][1] = current_shoulders_positions[0][1] - (BODY_HEIGHT / 2) * cos(dpitch);
    new_shoulders_positions[1][1] = current_shoulders_positions[1][1] + (BODY_HEIGHT / 2) * cos(dpitch);
    new_shoulders_positions[2][1] = current_shoulders_positions[2][1] - (BODY_HEIGHT / 2) * cos(dpitch);
    new_shoulders_positions[3][1] = current_shoulders_positions[3][1] + (BODY_HEIGHT / 2) * cos(dpitch);
    
  }
  
  new_shoulders_positions[0][2] = current_shoulders_positions[0][2] - (BODY_HEIGHT / 2) * sin(dpitch);
  new_shoulders_positions[1][2] = current_shoulders_positions[1][2] + (BODY_HEIGHT / 2) * sin(dpitch);
  new_shoulders_positions[2][2] = current_shoulders_positions[2][2] - (BODY_HEIGHT / 2) * sin(dpitch);
  new_shoulders_positions[3][2] = current_shoulders_positions[3][2] + (BODY_HEIGHT / 2) * sin(dpitch);
*/

  //On met à jour les inclinaisons
  current_body_inclinaisons[0] = roll;
  current_body_inclinaisons[1] = pitch;
  current_body_inclinaisons[2] = yaw;
}

void calculate_feet_positions() {

}

void set_shoulder_position(int leg_nb) {
  Serial.println("Update Shoulder");
  
  current_shoulders_positions[leg_nb][0] = new_shoulders_positions[leg_nb][0];
  current_shoulders_positions[leg_nb][1] = new_shoulders_positions[leg_nb][1];
  current_shoulders_positions[leg_nb][2] = new_shoulders_positions[leg_nb][2];
}

void MGI(int leg_nb, int x, int y, int z, float *alpha, float *beta, float *gamma) {
  int x0 = current_shoulders_positions[leg_nb][0];
  int y0 = current_shoulders_positions[leg_nb][1];
  int z0 = current_shoulders_positions[leg_nb][2];


  Serial.print("LEG NB : ");
  Serial.print(leg_nb);
  Serial.print("\tX0 : ");
  Serial.print(x0);
  Serial.print("\tY0 : ");
  Serial.print(y0);
  Serial.print("\tZ0 : ");
  Serial.println(z0);
  

  *alpha = atan2(y, x);

  float D = (pow((x - x0 - l1), 2) + pow((z - z0), 2) - pow(l2, 2) - pow(l3, 2)) / (2 * l2 * l3);

  if (abs(D) < 1) {
    *gamma = acos(D);

    float k1 = l2 + l3 * cos(*gamma);
    float k2 = l3 * sin(*gamma);

    *beta = atan2((k1 * (x - x0 - l1)) - (k2 * (z - z0)), (k2 * (x - x0 - l1)) + (k1 * (z - z0)));
  }
}

void compute_angles(int leg_nb, float *alpha, float *beta, float *gamma) {
  //Convertion en degrée
  *alpha *= (180 / pi);
  *beta *= (180 / pi);
  *gamma *= (180 / pi);

  //Ajout des offsets / inversions des angles en fonction de la patte
  switch (leg_nb) {
    case 0:
      *alpha = 180 - (*alpha + 90);
      *beta = 180 - (180 - *beta);
      *gamma = (180 - *gamma);
      break;

    case 1:
      *alpha = 180 - (*alpha + 90);
      *beta = 180 - (180 - *beta);
      *gamma = (180 - *gamma);
      break;

    case 2:
      *alpha = (*alpha + 90);
      *beta = (180 - *beta);
      *gamma = 180 - (180 - *gamma);
      break;

    case 3:
      *alpha = (*alpha + 90);
      *beta = (180 - *beta);
      *gamma = 180 - (180 - *gamma);
      break;
  }
}

void set_foot_position(int leg_nb) {
  float alpha;
  float beta;
  float gamma;

  int x = new_feet_positions[leg_nb][0];
  int y = new_feet_positions[leg_nb][1];
  int z = new_feet_positions[leg_nb][2];

  /*Serial.print("X : ");
  Serial.print(x);
  Serial.print("\tY : ");
  Serial.print(z);
  Serial.print("\tZ : ");
  Serial.println(z);*/

  MGI(leg_nb, x, y, z, &alpha, &beta, &gamma);
  compute_angles(leg_nb, &alpha, &beta, &gamma);

  //On lève la patte
  if (z > current_feet_positions[leg_nb][2]) {

    set_angle_servo(leg_nb, 1, beta);
    set_angle_servo(leg_nb, 2, gamma);
    set_angle_servo(leg_nb, 0, alpha);

    //On baisse la patte
  } else {

    set_angle_servo(leg_nb, 2, gamma);
    set_angle_servo(leg_nb, 1, beta);
    set_angle_servo(leg_nb, 0, alpha);

  }

  current_feet_positions[leg_nb][0] = x;
  current_feet_positions[leg_nb][1] = y;
  current_feet_positions[leg_nb][2] = z;
}

void update_positions() {

  //Mise à jour de la position des épaules

  //On parcourt les 4 épaules
  for (int i = 0; i < 4; i++) {

    //On parcourt les 3 coordonnées
    for (int j = 0; j < 3; j++) {

      if (new_shoulders_positions[i][j] != current_shoulders_positions[i][j]) {
        set_shoulder_position(i);
        set_foot_position(i);
      }
    }
  }

  // Mise à jour de la position des pattes

  //On parcourt les 4 pattes
  for (int i = 0; i < 4; i++) {

    //On parcourt les 3 coordonnées
    for (int j = 0; j < 3; j++) {

      if (new_feet_positions[i][j] != current_feet_positions[i][j])
        set_foot_position(i);
    }
  }
}

void place_feet_to(int leg_nb, int x, int y, int z) {
  new_feet_positions[leg_nb][0] = x;
  new_feet_positions[leg_nb][1] = y;
  new_feet_positions[leg_nb][2] = z;
}

void stand_up() {
  place_feet_to(0, 600, -600, -700);
  place_feet_to(1, 600, 600, -700);
  place_feet_to(2, 600, -600, -700);
  place_feet_to(3, 700, 700, -700);
}
