long timer;

void setup() {
  Serial.begin(9600);

  init_eyes();
  attach_servos();

  open_eyes();

  timer = millis();
  stand_up();
}

void loop() {
  if (millis() - timer > 1000) {
    if (millis() - timer > 2000) {
      timer = millis();
    }else {
      //calculate_shoulders_positions(5, 0, 0); 
    }
  }else {
    calculate_shoulders_positions(-10, 0, 0);  
  }

  update_positions();
}
