# Quadruped Roobot

Robot realised during the 2020 french lock-up

<img src="Images/IMG_20200429_114348.jpg" width="500" />

The main goal was for me to pratice my CAD skills as well as learning how robot walk.

## CAD

The CAD was done using **Solidworks 2021 SP4.0**

<img src="Images/Untitled.JPG" width="500" />

## Programming

The programmation was firstly done on an arduino. But a ROS2 package was also developped in order to do some simulation.

<img src="Images/Screenshot from 2023-08-02 08-47-08.png" width="500" />