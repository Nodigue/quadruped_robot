#!/usr/bin/env python3

from pathlib import Path

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch_ros.actions import Node

import xacro


def generate_launch_description() -> LaunchDescription:
    """
    Generate a launch description
    Will display the robot in RViz.
    """

    qr_description_package = Path(get_package_share_directory("qr_description"))

    # Read Xacro file
    xacro_file = qr_description_package / "urdf" / "quadruped_robot.xacro"
    assert xacro_file.exists(), "Can't find xacro file : " + xacro_file.as_posix()
    robot_description = xacro.process_file(xacro_file).toxml()

    # Create RVIZ Node
    rviz_config = qr_description_package / "rviz"

    rviz = Node(
        package="rviz2",
        executable="rviz2",
        arguments=["-d", str(rviz_config / "config.rviz")],
    )

    robot_state_publisher = Node(
        package="robot_state_publisher",
        executable="robot_state_publisher",
        parameters=[{"robot_description": robot_description}],
    )

    joint_controller = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["forward_position_controller"],
    )

    ld = LaunchDescription()
    ld.add_action(rviz)
    ld.add_action(robot_state_publisher)
    ld.add_action(joint_controller)

    return ld
