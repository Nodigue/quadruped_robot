#!/usr/bin/env python3

from pathlib import Path

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.actions import Node


def generate_launch_description() -> LaunchDescription:
    """
    Generate a launch description
    Will display the robot in RViz with a GUI to control the joint states.
    """

    qr_description_package = Path(get_package_share_directory("qr_description"))

    display_launch_description = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(str(qr_description_package / "launch/display.py"))
    )

    joint_state_publisher_gui = Node(
        package="joint_state_publisher_gui",
        executable="joint_state_publisher_gui",
        name="joint_state_publisher_gui",
        parameters=[],
    )

    ld = LaunchDescription()
    ld.add_action(display_launch_description)
    ld.add_action(joint_state_publisher_gui)

    return ld
