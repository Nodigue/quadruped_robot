import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource

def generate_launch_description():

    qr_gazebo_pkg = get_package_share_directory("qr_gazebo")
    qr_description_pkg = get_package_share_directory("qr_description")

    world = os.path.join(
        qr_gazebo_pkg,
        "worlds",
        "empty_world.world"
    )

    # Launch gazebo
    # arguments:
    # -s: Causes only the Gazebo server to run without the GUI client
    # -g: Runs the GUI client (cause a bug: the GUI stays black)
    # -r: Tells Gazebo to start running simulation immediately
    # -v4: Sets the verbosity level of Gazebo's console output
    ros_gz_sim = get_package_share_directory("ros_gz_sim")

    gzserver_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(ros_gz_sim, "launch", "gz_sim.launch.py")
        ),
        launch_arguments={"gz_args": f"{world}"}.items(),
    )

    # Display : robot_state_publisher + rviz
    display_launch = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(qr_description_pkg, "launch", "display.py")
        )
    )

    spawn_robot = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(qr_gazebo_pkg, "launch", 'spawn_robot.py')
        ),
    )

    ld = LaunchDescription()
    ld.add_action(gzserver_cmd)
    ld.add_action(display_launch)
    ld.add_action(spawn_robot)

    return ld
