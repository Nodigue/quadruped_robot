from pathlib import Path

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node

import xacro


def generate_launch_description():

    qr_gazebo_pkg = Path(get_package_share_directory("qr_gazebo"))

    # Read xacro file
    xacro_file = qr_gazebo_pkg / "urdf" / "quadruped_robot.gazebo.xacro"
    assert xacro_file.exists(), "Can't find xacro file : " + xacro_file.as_posix()
    robot_description = xacro.process_file(xacro_file).toxml()

    start_gazebo_ros_spawner_cmd = Node(
        package="ros_gz_sim",
        executable="create",
        arguments=[
            "-name",
            "quadruped_robot",
            "-string",
            robot_description,
            "-x",
            "0",
            "-y",
            "0",
            "-z",
            "0.5",
        ],
        output="screen",
    )

    bridge_params = qr_gazebo_pkg / "params/bridge.yaml"

    start_gazebo_ros_bridge_cmd = Node(
        package="ros_gz_bridge",
        executable="parameter_bridge",
        arguments=[
            "--ros-args",
            "-p",
            f"config_file:={str(bridge_params)}",
        ],
        output="screen",
    )

    ld = LaunchDescription()

    # Add any conditioned actions
    ld.add_action(start_gazebo_ros_spawner_cmd)
    ld.add_action(start_gazebo_ros_bridge_cmd)

    return ld
