cmake_minimum_required(VERSION 3.5)
project(qr_kinematics)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
    set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# Find dependencies
find_package(ament_cmake REQUIRED)
find_package(ament_cmake_python REQUIRED)

find_package(rclcpp REQUIRED)
find_package(rclpy REQUIRED)

find_package(geometry_msgs REQUIRED)
find_package(sensor_msgs REQUIRED)
find_package(visualization_msgs REQUIRED)

find_package(tf2 REQUIRED)
find_package(tf2_ros REQUIRED)

find_package(Eigen3 REQUIRED)

find_package(target REQUIRED)
find_package(qr_interfaces REQUIRED)

# Include Cpp "include" directory
include_directories(include)
include_directories(${EIGEN3_INCLUDE_DIR})

set(QR_SOURCES
  src/leg.cpp
  src/quadruped_robot.cpp
)

# Create Cpp executable
add_executable(walk
  ${QR_SOURCES}
  src/walk.cpp
)

ament_target_dependencies(walk
  rclcpp
  geometry_msgs
  sensor_msgs
  visualization_msgs
  tf2
  tf2_ros
  Eigen3
  target
  qr_interfaces
)

add_executable(body_IK
  ${QR_SOURCES}
  src/body_IK.cpp
)

ament_target_dependencies(body_IK
  rclcpp
  geometry_msgs
  sensor_msgs
  visualization_msgs
  tf2
  tf2_ros
  Eigen3
  target
  qr_interfaces
)

# Install Cpp executables
install(
  TARGETS walk
  DESTINATION lib/${PROJECT_NAME}
)

install(
  TARGETS body_IK
  DESTINATION lib/${PROJECT_NAME}
)

# Install folders
install(
  DIRECTORY launch
  DESTINATION share/${PROJECT_NAME}
)

# Install Python modules
ament_python_install_package(${PROJECT_NAME})

# Install Python executables
install(
  PROGRAMS
    scripts/joy_controller.py
    scripts/gait_timer.py
    scripts/joy_visualizer.py
    scripts/push_up.py
  DESTINATION
    lib/${PROJECT_NAME}
)

ament_package()