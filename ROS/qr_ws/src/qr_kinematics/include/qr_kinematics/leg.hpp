#ifndef LEG_H
#define LEG_H

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>

#include <Eigen/Core>

class Leg
{

public:
	static constexpr unsigned int NB_JOINT = 3;

public:
	Leg(const unsigned int& p_ID, const Eigen::Vector3d& p_lengths, const Eigen::Matrix4d& p_shoulderPose);
	~Leg();

	inline Eigen::Vector3d getLengths() const { return m_lengths; }
	inline void setLengths(const Eigen::Vector3d& p_lengths) { m_lengths = p_lengths; }

	inline double getJointValue(const unsigned int& p_index) const { return m_q(p_index); };
	inline Eigen::Vector3d getJointValues() const { return m_q; }
	inline void setJointValue(const unsigned int& p_index, const double& p_value) { m_q(p_index) = p_value; }
	inline void setJointValues(const Eigen::Vector3d& p_jointValues) { m_q = p_jointValues; }

	inline Eigen::Matrix4d getShoulderPose() const { return m_shoulderPose; };
	inline void setShoulderPose(const Eigen::Matrix4d& p_shoulderPose) { m_shoulderPose = p_shoulderPose; };

	// Forward Kinematics
	Eigen::Matrix4d FK(const int& p_index = -1) const;

	// Get full pose of the feet in the shoulder frame
	inline Eigen::Matrix4d getShoulderToFeetPose() const { return m_feetCurrentPose; }

	// Get full pose of the feet in the body frame
	inline Eigen::Matrix4d getBodyToFeetPose() const { return m_shoulderPose * m_feetCurrentPose; }

	// Get feet position in the body frame (implicit)
	inline Eigen::Vector3d getFeetPosition() const { return this->getBodyToFeetPose().block<3, 1>(0, 3); }

	// Inverse Kinematics
	bool Shoulder_IK(const Eigen::Vector3d& p_targetPosition);
	bool Body_IK(const Eigen::Matrix4d& p_targetPose);
	bool move(const Eigen::Vector3d& p_dX);

private:
	unsigned int m_ID;

	Eigen::Vector3d m_lengths;

	// Joint Values
	Eigen::Vector3d m_q;

	// References frames
	std::vector<Eigen::Matrix4d> m_relativeFrames;

	// Pose of the shoulder and of the feet
	Eigen::Matrix4d m_shoulderPose;
	Eigen::Matrix4d m_feetInitialPose;
	Eigen::Matrix4d m_feetCurrentPose;
};

#endif