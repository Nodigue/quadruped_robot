#ifndef QUADRUPED_ROBOT_H
#define QUADRUPED_ROBOT_H

#include "qr_kinematics/leg.hpp"

#include "qr_interfaces/msg/gait.hpp"
#include "qr_interfaces/msg/foot_position.hpp"

#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <visualization_msgs/msg/marker.hpp>

#include <Eigen/Core>

#include <vector>

struct GaitParameters
{
	double gaitTime = 0;
	unsigned int currentStep = 0;

	// Unit : mm
	double xMin = 0;
	double xMax = 0;

	Eigen::Vector3d currentPosition = Eigen::Vector3d::Zero();
	Eigen::Vector3d targetPosition = Eigen::Vector3d::Zero();
};

class QuadrupedRobot
{
public:
	QuadrupedRobot();
	~QuadrupedRobot();

	void freeze();
	bool fixedIK(const Eigen::Matrix4d& p_targetPose);

	double getPivotPosition(double p_angularVel, double p_eps = 1e-6) const;

	void bodyGait(const double& p_t);

	void legGait2(int p_ID, double p_t, double p_linearVel, double p_angularVel);
	void legGait(const double& p_t, const unsigned int& p_ID, const GaitParameters& p_gaitParams, const double& p_linearVel, const double& p_angularVel);
	void gait(const qr_interfaces::msg::Gait::SharedPtr p_gaitTime, const double& p_linearVel, const double& p_angularVel);

	void update();
	void publish();
	void updateAndPublish();

	visualization_msgs::msg::Marker getMarker(const Eigen::Vector3d& p_pivotPosition);

private:

	// ROS Node
	rclcpp::Node m_node;

	sensor_msgs::msg::JointState m_jointsState;
	rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr m_publisher;
	rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr m_debugPub;

	rclcpp::Publisher<qr_interfaces::msg::FootPosition>::SharedPtr m_footPositionPublisher;

	void defineJointsState();

	unsigned int m_nbLegs;

	void addLeg(const unsigned int& p_ID);
	Eigen::Matrix4d getLegPose(const unsigned int& p_ID);

	std::vector<Leg> m_legs;
	std::vector<Eigen::Matrix4d> m_legInitialPose;

	std::vector<Eigen::Matrix4d> m_freezeLegsPose;

	// Gait parameters
	std::vector<double> m_xRanges;
	std::vector<double> m_zRanges;

	double m_lastT;

	GaitParameters m_gaitParams;
	bool m_stepFlag;
};

#endif