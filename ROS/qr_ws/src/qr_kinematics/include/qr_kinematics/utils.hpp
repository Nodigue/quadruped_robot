#ifndef QUADRUPED_ROBOT_UTILS
#define QUADRUPED_ROBOT_UTILS

#include "geometry_msgs/msg/vector3.hpp"
#include "geometry_msgs/msg/transform.hpp"

#include <Eigen/Core>
#include <Eigen/Geometry>

#define _USE_MATH_DEFINES
#include <cmath>

namespace QR_Utils
{

    template <typename T>
    int sign(T val)
    {
        return (T(0) < val) - (val < T(0));
    }

    /**
     * @brief Wrap a number in a specific range [min; max].
     *        https://stackoverflow.com/questions/28313558/how-to-wrap-a-number-into-a-range
     *
     * @param value Number to wrap
     * @param p_min Range minimum value
     * @param p_max Range maximum value
     * @return T Wrapped number
     */
    template <typename T>
    T wrap(const T& value, const T& p_min, const T& p_max)
    {
        return p_min + std::fmod((value - p_min), (p_max - p_min));
    }

    template <typename T>
    Eigen::Matrix<T, 4, 4> translation(const Eigen::Matrix<T, 3, 1>& p_axis, const T& p_value)
    {
        Eigen::Matrix<T, 4, 4> TM = Eigen::Matrix<T, 4, 4>::Identity();
        TM.template block<3, 1>(0, 3) = p_axis * p_value;

        return TM;
    }

    template <typename T>
    Eigen::Matrix<T, 4, 4> TransXYZ(const T& p_Tx, const T& p_Ty, const T& p_Tz)
    {
        Eigen::Matrix<T, 4, 4> TM = Eigen::Matrix<T, 4, 4>::Identity();
        TM(0, 3) = p_Tx;
        TM(1, 3) = p_Ty;
        TM(2, 3) = p_Tz;

        return TM;
    }

    template <typename T>
    Eigen::Matrix<T, 4, 4> rotation(const Eigen::Matrix<T, 3, 1>& p_axis, const T& p_value)
    {
        Eigen::Matrix<T, 4, 4> TM = Eigen::Matrix<T, 4, 4>::Identity();
        Eigen::AngleAxis<T> rot = Eigen::AngleAxis<T>(p_value, p_axis);
        TM.template block<3, 3>(0, 0) = rot.toRotationMatrix();

        return TM;
    }

    template <typename T>
    Eigen::Matrix<T, 4, 4> MDH(const T& p_alpha, const T& p_a, const T& p_theta, const T& p_d)
    {
        Eigen::Matrix<T, 4, 4> rotX = rotation<T>(Eigen::Matrix<T, 3, 1>::UnitX(), p_alpha);
        Eigen::Matrix<T, 4, 4> transX = translation<T>(Eigen::Matrix<T, 3, 1>::UnitX(), p_a);
        Eigen::Matrix<T, 4, 4> rotZ = rotation<T>(Eigen::Matrix<T, 3, 1>::UnitZ(), p_theta);
        Eigen::Matrix<T, 4, 4> transZ = translation<T>(Eigen::Matrix<T, 3, 1>::UnitZ(), p_d);

        return rotX * transX * rotZ * transZ;
    }

    template <typename T>
    Eigen::Matrix<T, 4, 4> tf2eigen(const geometry_msgs::msg::Transform& p_tf)
    {
        Eigen::Matrix<T, 4, 4> TM = Eigen::Matrix<T, 4, 4>::Identity();

        // Position
        Eigen::Matrix<T, 3, 1> posV(p_tf.translation.x, p_tf.translation.y, p_tf.translation.z);

        // Rotation
        Eigen::Quaternion<T> quat(p_tf.rotation.w, p_tf.rotation.x, p_tf.rotation.y, p_tf.rotation.z);
        Eigen::Matrix<T, 3, 3> rotM = quat.matrix();

        TM.template block<3, 1>(0, 3) = posV;
        TM.template block<3, 3>(0, 0) = rotM;

        return TM;
    }

    template <typename T>
    geometry_msgs::msg::Vector3 eigenToVector3(const Eigen::Matrix<double, 3, 1>& p_eigenVector)
    {
        geometry_msgs::msg::Vector3 vector;

        vector.x = p_eigenVector(0);
        vector.y = p_eigenVector(1);
        vector.z = p_eigenVector(2);

        return vector;
    }
}

#endif