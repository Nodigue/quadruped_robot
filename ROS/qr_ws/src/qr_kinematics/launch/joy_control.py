#!/usr/bin/env python3

from launch_ros.actions import Node
from launch import LaunchDescription

def generate_launch_description():

    # Joy Node
    joy_node = Node(
        package = "joystick_ros2",
        executable = "joystick_ros2"
    )

    # Joy Controller
    joy_controller_node = Node(
        package = "qr_kinematics",
        executable = "joy_controller.py"
    )

    # Gait timer
    gait_timer_node = Node(
        package = "qr_kinematics",
        executable = "gait_timer.py"
    )

    return LaunchDescription([
        joy_node,
        joy_controller_node,
        gait_timer_node
    ])