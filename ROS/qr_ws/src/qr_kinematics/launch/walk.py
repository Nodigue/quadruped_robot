#!/usr/bin/env python3

from pathlib import Path

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource

def generate_launch_description():

    # Display : robot_state_publisher + rviz
    qr_description_pkg = Path(get_package_share_directory("qr_description"))

    display_launch_file = qr_description_pkg / "launch" / "display.py"
    display_launch = IncludeLaunchDescription(PythonLaunchDescriptionSource(str(display_launch_file)))

    # Joy Control
    qr_kinematics_pkg = Path(get_package_share_directory("qr_kinematics"))

    #joy_control_launch_file = qr_kinematics_pkg / "launch" / "joy_control.py"
    #joy_control_launch = IncludeLaunchDescription(PythonLaunchDescriptionSource(str(joy_control_launch_file)))

    # Robot controller
    walk_node = Node(
        package = "qr_kinematics",
        executable = "body_IK",
        output = "screen",
        emulate_tty = True,
    )

    target_node = Node(
        package = "target",
        executable = "spawn_target",
        output = "screen"
    )

    return LaunchDescription([
        display_launch,
        #joy_control_launch,
        walk_node,
        target_node
    ])