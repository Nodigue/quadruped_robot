#!/usr/bin/env python3

import math
import numpy as np
import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32
from qr_interfaces.msg import Gait

class GaitTimer(Node):

    def __init__(self) -> None:
        super().__init__("qr_gait_timer")

        self.gait_time_control = 0.0
        self.subscriber = self.create_subscription(Float32, "/qr/gait_time_control", self.twist_callback, 1)

        self.publisher = self.create_publisher(Gait, "qr/gait_time", 10)
        self.publiser_timer = self.create_timer(1 / 200, self.publish)

        # Controller parameters
        self.paused = False

        self.gait_time_full = 0.0
        self.last_gait_time_full = 0.0

        self.gait_step = 0
        self.new_step_flag = False
        self.gait_time = 0.0

    def twist_callback(self, msg) -> None:
        """
        Callback function when a new gait_time_control message is received.
        """

        self.gait_time_control = float(msg.data)

    def update(self) -> None:
        if not self.paused:
            self.gait_time_full += (0.01) * self.gait_time_control

            if (math.floor(self.gait_time_full) != math.floor(self.last_gait_time_full)):
                self.gait_step += 1
                self.new_step_flag = True
            else:
                self.new_step_flag = False

            if (self.gait_time_full >= 4):
                self.gait_time_full = 0.0
                self.gait_step = 0.0

        else:
            self.gait_time_full = 0.0

        self.gait_time = self.gait_time_full - math.floor(self.gait_time_full)
        self.last_gait_time_full = self.gait_time_full

    def publish(self) -> None:

        # Update internal state
        self.update()

        # Define a new message
        gait_msg = Gait()

        gait_msg.gait_time_full = float(self.gait_time_full)
        gait_msg.gait_step = int(self.gait_step)
        gait_msg.new_step_flag = bool(self.new_step_flag)
        gait_msg.gait_time = float(self.gait_time)

        # Publish the message
        self.publisher.publish(gait_msg)

def main(args = None) -> None:
    rclpy.init(args = args)

    gait_timer = GaitTimer()

    rclpy.spin(gait_timer)

    gait_timer.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()