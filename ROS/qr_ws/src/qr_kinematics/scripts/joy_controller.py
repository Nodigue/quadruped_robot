#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

class JoyController(Node):

    def __init__(self) -> None:
        super().__init__("joy_controller")

        # Input : /joy
        self.subscription = self.create_subscription(Joy, "joy", self.callback, 10)

        # Output : /qr_gait_time_speed & /qr_twist
        self.gait_time_control = Float32()
        self.gait_time_control_publisher = self.create_publisher(Float32, "qr/gait_time_control", 10)

        self.cmd_twist = Twist()
        self.cmd_twist_publisher = self.create_publisher(Twist, "qr/cmd_twist", 10)

        self.publisher_timer = self.create_timer(0.01, self.publish)

    def callback(self, data) -> None:
        """
        Callback function called when a new joy message is received. It converts
        the joy message data to a Control message.
        """

        # Here I used the package : https://github.com/FurqanHabibi/joystick_ros2
        # So the axes can be wrong if you use another package.

        self.gait_time_control.data = (-data.axes[4] + 1) / 2

        self.cmd_twist.linear.x = data.axes[5]
        self.cmd_twist.angular.z = -data.axes[2]

    def publish(self) -> None:
        """
        Publish function called every 0.01sec. Publish a new Control message
        containing information about the gait time speed (controlled by the RT
        trigger) and the twist of the robot (controlled by the left joystick).
        """

        self.gait_time_control_publisher.publish(self.gait_time_control)
        self.cmd_twist_publisher.publish(self.cmd_twist)

def main(args = None) -> None:
    rclpy.init(args = args)

    joy_controller = JoyController()

    rclpy.spin(joy_controller)

    joy_controller.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()