#!/usr/bin/env python3

import rclpy

import sensor_msgs.msg as sensor_msgs

import math
import numpy as np
import geometry.rotation as rotation
import geometry.transformation as transformation

import tkinter as tk
import gui.tkinter_utils as tk_utils

class JoyVisualizer(tk.Tk):

    def __init__(self, title = "App", width : int = 600, height : int = 600):
        super().__init__()

        self.create_window(title, width, height)
        self.create_canvas()

        # Create a rclpy node
        self.node = rclpy.create_node("joy_visualizer")
        self.subscription = self.node.create_subscription(sensor_msgs.Joy, "joy", self.callback, 10)

        # Joy input
        self.px = np.zeros(3)
        self.py = np.zeros(3)
        self.direction_angle = 0

        # Test
        self.L = 50
        self.p1 = np.array([50, self.L, 0])
        self.p2 = self.p1 + np.array([50, 0, 0])

    def create_window(self, title : str, width : int, height : int) -> None:
        """Create a tkinter window object."""

        # Store attributes
        self.width = width
        self.height = height

        # Create window
        self.title(title)

        self.geometry(f'{width}x{height}')
        self.resizable(False, False)

        # Widgets

        # Bindings

        # Make sure the window has focus
        self.focus_set()

    def create_canvas(self) -> None:
        """Create a tkinter canvas and add it to the window"""

        # Drawing GUI Canvas
        border_offset = (10, 10)

        self.canvas = tk_utils.Canvas(self, self.width - (2 * border_offset[0]), self.height - (2 * border_offset[1]))
        self.canvas.place(x = border_offset[0], y = border_offset[1])

        # Define a reference frame
        p = np.array([self.width / 2, self.height / 2, 0])
        R = rotation.rotX(math.pi) @ rotation.rotZ(math.pi / 2)

        self.canvas.reference_frame = transformation.compose(p, R)

    def callback(self, data) -> None:
        """Callback function when new joy data is received."""

        # Get point from the joystick
        self.px = np.array([data.axes[4], 0, 0]) * 200
        self.py = np.array([0, data.axes[3], 0]) * 200

        point = self.px + self.py

        # Get direction angle
        threshold = 1e-5

        if (abs(point[0]) > threshold or abs(point[1]) > threshold):
            self.direction_angle = math.atan2(point[1], point[0])
            self.direction_angle = self.direction_angle
        else:
            self.direction_angle = 0

        # Get curvature angle
        if (abs(data.axes[3]) < threshold):
            curvature = threshold;
        else:
            curvature = data.axes[3]

        if (curvature >= 0.0):
            cx = math.tan((2.0 - curvature) * 3.1415 / 4.0) * 50.0;

        elif (curvature < 0.0):
            cx = math.tan((2.0 - curvature) * 3.1415 / 4.0) * 50.0;

        print(f"Curvature : {curvature} / cx : {cx}")

        maxdist = 0.0;
        sweepwangle = 0.0;

        for i in range(0, 4):
            dist = 0#math.sqrt((cx - legx0[i]) * (cx-legx0[i]) + legy0[i]*legy0[i]);

            if (dist > maxdist):
                maxdist = dist;

        sweepwangle = np.sign(cx) * 25.0 / maxdist;


    def render(self) -> None:

        # Clear the canvas
        self.canvas.delete("all")

        tk_utils.draw_point(self.canvas, self.px, 20)
        tk_utils.draw_point(self.canvas, self.py, 20)

        tk_utils.draw_point(self.canvas, self.p1, 5, "Red")
        tk_utils.draw_point(self.canvas, self.p2, 5, "Blue")

        R1 = self.py[1] - self.L
        R2 = self.py[1] + self.L

        tk_utils.draw_arc(self.canvas, self.py, R1, 0, 100, 2)
        tk_utils.draw_arc(self.canvas, self.py, R2, 0, 100, 2)


        pose = transformation.rotZ(self.direction_angle)
        pose = self.canvas.to_canvas(pose)

        tk_utils.drawCoordinateSystem(self.canvas, pose)

        # Render reference frame
        tk_utils.drawCoordinateSystem(self.canvas, self.canvas.reference_frame)

    def update(self) -> None:

        # Update the tkinter window
        super().update()

        # Spin the node
        rclpy.spin_once(self.node, timeout_sec=1)

        # Re-render
        self.render()

def main(args=None):
    rclpy.init(args=args)

    joy_visualizer = JoyVisualizer()

    while rclpy.ok():
        joy_visualizer.update()

    rclpy.shutdown()


if __name__ == '__main__':
    main()