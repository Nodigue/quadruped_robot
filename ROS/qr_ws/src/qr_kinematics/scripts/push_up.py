#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray

import math


class Publisher(Node):

    def __init__(self):
        super().__init__("publisher")

        self.publisher_ = self.create_publisher(
            Float64MultiArray,
            "forward_position_controller/commands",
            10,
        )

        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0

    def timer_callback(self):
        msg = Float64MultiArray()

        q2 = math.cos(self.i)
        q3 = math.sin(self.i)
        self.i += 1

        msg.data = [0, q2, q3, 0, q2, q3, 0, q2, q3, 0, q2, q3]

        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%s"' % msg.data)


def main(args=None):
    rclpy.init(args=args)
    publisher = Publisher()

    rclpy.spin(publisher)

    publisher.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
