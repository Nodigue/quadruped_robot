#include "qr_kinematics/leg.hpp"
#include "qr_kinematics/quadruped_robot.hpp"
#include "qr_interfaces/msg/gait.hpp"

#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <memory>
#include <chrono>

using namespace std::chrono_literals;

class Controller : public rclcpp::Node
{
public:
    Controller():
        Node("qr_controller")
    {
        m_tfBuffer = std::make_unique<tf2_ros::Buffer>(this->get_clock());
        m_tfListener = std::make_shared<tf2_ros::TransformListener>(*m_tfBuffer);

        m_timer = this->create_wall_timer(1ms, std::bind(&Controller::on_timer, this));
    }

    ~Controller() {}

    void update()
    {
        m_robot.updateAndPublish();
    }

private:
    QuadrupedRobot m_robot;

    std::unique_ptr<tf2_ros::Buffer> m_tfBuffer;
    std::shared_ptr<tf2_ros::TransformListener> m_tfListener;
    rclcpp::TimerBase::SharedPtr m_timer;

private:

    void on_timer()
    {
        std::string fromFrameRel = "base_link";
        std::string toFrameRel = "target";

        geometry_msgs::msg::TransformStamped tfStamped;

        try
        {
            tfStamped = m_tfBuffer->lookupTransform(toFrameRel, fromFrameRel, tf2::TimePointZero);
        }
        catch (tf2::TransformException& ex)
        {
            RCLCPP_INFO(this->get_logger(), "Could not transform %s to %s: %s", toFrameRel.c_str(), fromFrameRel.c_str(), ex.what());
            return;
        }

        Eigen::Matrix4d targetPose = Eigen::Matrix4d::Identity();

        Eigen::Vector3d posV(tfStamped.transform.translation.x, tfStamped.transform.translation.y, tfStamped.transform.translation.z);
        Eigen::Quaterniond quat(tfStamped.transform.rotation.w, tfStamped.transform.rotation.x, tfStamped.transform.rotation.y, tfStamped.transform.rotation.z);

        targetPose.block<3, 1>(0, 3) = posV;
        targetPose.block<3, 3>(0, 0) = quat.toRotationMatrix();

        // Convert to eigen matrix
        m_robot.fixedIK(targetPose);
    }
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<Controller> controller = std::make_shared<Controller>();

    while (rclcpp::ok())
    {
        controller->update();
        rclcpp::spin_some(controller);
    }

    rclcpp::shutdown();

    return 0;
}