#include "qr_kinematics/leg.hpp"
#include "qr_kinematics/utils.hpp"
#include <iostream>

Leg::Leg(const unsigned int& p_ID, const Eigen::Vector3d& p_lengths, const Eigen::Matrix4d& p_shoulderPose):
    m_ID(p_ID),
    m_lengths(p_lengths),
    m_q(Eigen::Vector3d::Zero()),
    m_shoulderPose(p_shoulderPose),
    m_feetInitialPose(Eigen::Matrix4d::Identity()),
    m_feetCurrentPose(Eigen::Matrix4d::Identity())
{
    // Define relative frames (Using modified denavit-hartenberg parameters)
    m_relativeFrames.push_back(QR_Utils::MDH<double>(0,         0,				0,			0));
    m_relativeFrames.push_back(QR_Utils::MDH<double>(-M_PI_2,	m_lengths(0),	0,			0));
    m_relativeFrames.push_back(QR_Utils::MDH<double>(0,			m_lengths(1),	M_PI_2,		0));
    m_relativeFrames.push_back(QR_Utils::MDH<double>(0,			m_lengths(2),	0,			0));

    // Solve the forward kinematics
    m_feetCurrentPose = FK();
    m_feetInitialPose = m_feetCurrentPose;
}

Leg::~Leg() {}

/**
 * @brief Recursive Forward Kinematics function.
 *
 * @param p_index Index of the frame. (If index = -1 then we solve for the last frame).
 * @return Eigen::Matrix4d Pose of the selected frame in the shoulder frame.
 */
Eigen::Matrix4d Leg::FK(const int& p_index) const
{
    int index = p_index;
    if (index == -1)
    {
        index = NB_JOINT + 1;
    }

    Eigen::Matrix4d pose_i = Eigen::Matrix4d::Identity();

    for (int i = 0; i < index; ++i)
    {
        if (i < 3) pose_i *= m_relativeFrames[i] * QR_Utils::rotation<double>(Eigen::Vector3d::UnitZ(), m_q(i));
        else pose_i = pose_i * m_relativeFrames[i];
    }

    return pose_i;
}

/**
 * @brief Analytical Inverse Kinematics function.
 *
 * @param p_targetPosition Target position of the feet defined in the shoulder frame.
 * @return true, if a solution was found.
 * @return false, if no solution were found (unreachable target position).
 */
bool Leg::Shoulder_IK(const Eigen::Vector3d& p_targetPosition)
{
    bool success = true;

    m_q(0) = atan2(p_targetPosition(1), p_targetPosition(0));

    double r = 0;
    if (abs(cos(m_q(0))) >= (sqrt(2) / 2)) r = p_targetPosition(0) / cos(m_q(0));
    else r = p_targetPosition(1) / sin(m_q(0));

    r = r - m_lengths(0);

    double L = sqrt(r * r + p_targetPosition(2) * p_targetPosition(2));
    double A = (L * L - m_lengths(1) * m_lengths(1) - m_lengths(2) * m_lengths(2)) / (2 * m_lengths(1) * m_lengths(2));

    // Target position is unreachable
    if (A < -1)
    {
        success = false;
        A = -1;
    }
    else if (A > 1)
    {
        success = false;
        A = 1;
    }

    double g1 = atan2(p_targetPosition(2), r);
    double g2 = atan2(m_lengths[2] * sqrt(1 - A * A), m_lengths(1) + m_lengths(2) * A);

    m_q(1) = -(g1 + g2);
    m_q(2) = atan2(sqrt(1 - A * A), A) - (M_PI / 2);

    // Update the current position
    m_feetCurrentPose = FK();

    return success;
}

bool Leg::Body_IK(const Eigen::Matrix4d& p_targetPose)
{
    // Compute target position in shoulder frame
    Eigen::Matrix4d shoulderToTarget = m_shoulderPose.inverse() * p_targetPose;
    Eigen::Vector3d targetPosition = shoulderToTarget.block<3, 1>(0, 3);

    // Solve
    return this->Shoulder_IK(targetPosition);
}

bool Leg::move(const Eigen::Vector3d& p_dX)
{
    std::cout << "p_dX : " << p_dX.transpose() << std::endl;
    /*
    Eigen::Vector3d targetPosition = m_currentPosition + p_dX;
    return Shoulder_IK(targetPosition);
    */

   return true;
}