#include "qr_kinematics/quadruped_robot.hpp"
#include "qr_kinematics/utils.hpp"

#define _USE_MATH_DEFINES
#include <cmath>

#include <string>

QuadrupedRobot::QuadrupedRobot():
    m_node("quadruped_robot")
{
    m_nbLegs = 4;

    for (unsigned int i = 0; i < m_nbLegs; ++i)
    {
        this->addLeg(i);
    }

    // Define gait parameters
    for (unsigned int i = 0; i < m_nbLegs; ++i)
    {
        m_xRanges.push_back(100 * 0.001);
        m_zRanges.push_back(30 * 0.001);
    }

    // Define ROS publisher
    m_publisher = m_node.create_publisher<sensor_msgs::msg::JointState>("joint_states", 10);
    m_footPositionPublisher = m_node.create_publisher<qr_interfaces::msg::FootPosition>("qr_foot_position", 10);
    m_debugPub = m_node.create_publisher<visualization_msgs::msg::Marker>("debug", 10);

    // Define joint states
    this->defineJointsState();

    // Store initial shoulderToFeet
    this->freeze();
}

QuadrupedRobot::~QuadrupedRobot() {}

void QuadrupedRobot::defineJointsState()
{
    std::vector<std::string> jointsName = {"joint_1_1", "joint_1_2", "joint_1_3",
                                           "joint_2_1", "joint_2_2", "joint_2_3",
                                           "joint_3_1", "joint_3_2", "joint_3_3",
                                           "joint_4_1", "joint_4_2", "joint_4_3"
                                          };

    size_t nbJoints = jointsName.size();

    m_jointsState.name.resize(nbJoints);
    m_jointsState.position.resize(nbJoints);

    for (size_t i = 0; i < nbJoints; i++)
    {
        m_jointsState.name[i] = jointsName[i];
        m_jointsState.position[i] = 0.0;
    }
}

void QuadrupedRobot::addLeg(const unsigned int& p_ID)
{
    // Leg lengths
    Eigen::Vector3d legLengths;
    legLengths << 0.0460, 0.0301, 0.0860;

    // Leg offset
    double radiusOffset = 0.060251;
    double angleOffset = 0.876808;
    double zOffset = 0.023550;

    double x = radiusOffset * cos(angleOffset + ((p_ID + 1) * M_PI / 2));
    double y = radiusOffset * sin(angleOffset + ((p_ID + 1) * M_PI / 2));
    double z = zOffset;
    Eigen::Vector3d positionVector(x, y, z);

    Eigen::AngleAxisd roll(0, Eigen::Vector3d::UnitX());
    Eigen::AngleAxisd yaw(0, Eigen::Vector3d::UnitY());
    Eigen::AngleAxisd pitch((M_PI / 4) + ((p_ID + 1) * M_PI / 2), Eigen::Vector3d::UnitZ());

    Eigen::Quaternion<double> q = roll * yaw * pitch;
    Eigen::Matrix3d rotationMatrix = q.matrix();

    Eigen::Matrix4d offset = Eigen::Matrix4d::Identity();
    offset.block<3, 1>(0, 3) = positionVector;
    offset.block<3, 3>(0, 0) = rotationMatrix;

    // Add leg to the robot
    m_legs.push_back(Leg(p_ID, legLengths, offset));

    // Get leg initial position
    Eigen::Matrix4d legPose = this->getLegPose(p_ID);
    m_legInitialPose.push_back(legPose);
}

/**
 * @brief Return the pose of the feet of a leg in the body frame
 *
 * @param p_ID ID of the leg
 * @return Eigen::Matrix4d Pose of the feet
 */
Eigen::Matrix4d QuadrupedRobot::getLegPose(const unsigned int& p_ID)
{
    if (p_ID > m_nbLegs)
    {
        throw std::range_error("Leg ID out of range");
    }

    // Return the pose of the feet in the body frame
    return m_legs[p_ID].getBodyToFeetPose();
}

void QuadrupedRobot::freeze()
{
    m_freezeLegsPose.clear();

    // Store legs current pose
    for (unsigned int i = 0; i < m_nbLegs; i++)
    {
        Eigen::Matrix4d legPose = m_legs[i].getShoulderToFeetPose();
        m_freezeLegsPose.push_back(legPose);
    }
}

bool QuadrupedRobot::fixedIK(const Eigen::Matrix4d& p_targetPose)
{
    bool success = true;

    for (unsigned int i = 0; i < m_nbLegs; i++)
    {
        Eigen::Matrix4d legOffset = m_legs[i].getShoulderPose();

        Eigen::Matrix4d legFreezePose = m_freezeLegsPose[i];
        Eigen::Matrix4d T = legOffset.inverse() * p_targetPose.inverse() * legOffset * legFreezePose;

        success = success && m_legs[i].Shoulder_IK(T.block<3, 1>(0, 3));
    }

    return success;
}

double QuadrupedRobot::getPivotPosition(double p_angularVel, double p_eps) const
{
    double cx = 0;

    if (abs(p_angularVel) < p_eps)
    {
        cx = tan((1 - p_eps) * (M_PI / 2));
    }
    else
    {
        cx = tan((1 - p_angularVel) * (M_PI / 2));
    }

    return cx;
}

void QuadrupedRobot::bodyGait(const double& p_t)
{
    Eigen::Matrix4d targetPose = Eigen::Matrix4d::Identity();

    // TODO : Potentially add an offset to match with leg sequence
    targetPose(1, 3) = 0.05 * sin(p_t * M_PI);

    this->fixedIK(targetPose);
}

void QuadrupedRobot::legGait2(int p_ID, double p_t, double p_linearVel, double p_angularVel)
{
    double x0 = m_legInitialPose[p_ID](0, 3);
    double x = 0;

    double y0 = m_legInitialPose[p_ID](1, 3);
    double y = 0;

    Eigen::Matrix4d targetPose = Eigen::Matrix4d::Identity();

    // Compute pivot point
    double cx = this->getPivotPosition(p_angularVel);
    double r = sqrt((x0 - cx) * (x0 - cx) + (y0) * (y0));

    Eigen::Vector3d pivotPosition(0, cx, 0);

    //visualization_msgs::msg::Marker marker = this->getMarker(pivotPosition);
    //m_debugPub->publish(marker);

    double theta0 = atan2(y0, x0 - cx);

    if (p_t < 1)
    {
        double t = (theta0 - (p_linearVel * p_t)) / r;

        x = cx + r * cos(t);
        y = r * sin(t);
    }
    else
    {
        // Come back
        double t = (p_t - 1.0) / 3.0;
        x = t;
    }

    Eigen::Matrix4d tx = QR_Utils::translation<double>(Eigen::Vector3d::UnitX(), x);
    Eigen::Matrix4d ty = QR_Utils::translation<double>(Eigen::Vector3d::UnitY(), y);
    Eigen::Matrix4d tz = QR_Utils::translation<double>(Eigen::Vector3d::UnitZ(), 0);

    targetPose = tx * ty * tz;

    m_legs[p_ID].Body_IK(targetPose);
}

void QuadrupedRobot::legGait(const double& p_t, const unsigned int& p_ID, const GaitParameters& p_gaitParams, const double& p_linearVel, const double& p_angularVel)
{
    // Define gait target pose
    Eigen::Matrix4d targetPose = Eigen::Matrix4d::Identity();

    double x = 0;
    double y = 0;
    double z = p_linearVel;

    double xMin = p_gaitParams.xMin;
    double xMax = p_gaitParams.xMax;

    double angle = (1 - p_angularVel) * (M_PI / 2);
    std::cout << angle << "\n";

    if (p_t < 1)
    {
        // Take a step
        double t = p_t;
        x = (xMax - xMin) * sin(t * angle) + xMin;
        y = cos(t * angle);
        z = m_zRanges[p_ID] * sin(t * M_PI);
    }
    else
    {
        // Come back
        double t = (p_t - 1.0) / 3.0;
        x = ((xMin - xMax) * t) + xMax;
        z = 0;
    }

    Eigen::Matrix4d tx = QR_Utils::translation<double>(Eigen::Vector3d::UnitX(), x);
    Eigen::Matrix4d ty = QR_Utils::translation<double>(Eigen::Vector3d::UnitY(), y);
    Eigen::Matrix4d tz = QR_Utils::translation<double>(Eigen::Vector3d::UnitZ(), z);

    targetPose = tx * ty * tz * m_legInitialPose[p_ID];

    m_legs[p_ID].Body_IK(targetPose);
}

void QuadrupedRobot::gait(const qr_interfaces::msg::Gait::SharedPtr p_gaitTime, const double& p_linearVel, const double& p_angularVel)
{
    // If we pass into a new step
    if (p_gaitTime->new_step_flag)
    {
        // we then update the gait parameters with the current linear speed
        m_gaitParams.xMin = -0.05 * p_linearVel + p_angularVel;
        m_gaitParams.xMax = 0.05 * p_linearVel;
    }

    // Update body position
    //this->bodyGait(p_gaitTime->gait_time_full);

    // Update legs positions
    this->legGait2(0, QR_Utils::wrap<double>(p_gaitTime->gait_time + 0.0, 0, 4), p_linearVel, p_angularVel);
    //this->legGait(QR_Utils::wrap<double>(p_gaitTime->gait_time + 1.0, 0, 4), 2, m_gaitParams, p_linearVel, p_angularVel);
    //this->legGait(QR_Utils::wrap<double>(p_gaitTime->gait_time + 2.0, 0, 4), 1, m_gaitParams, p_linearVel, p_angularVel);
    //this->legGait(QR_Utils::wrap<double>(p_gaitTime->gait_time + 3.0, 0, 4), 3, m_gaitParams, p_linearVel, p_angularVel);
}

/**
 * @brief Update the current joints state of the robot
 *
 */
void QuadrupedRobot::update()
{
    for (unsigned int i = 0; i < m_nbLegs; ++i)
    {
        for (unsigned int j = 0; j < 3; ++j)
        {
            m_jointsState.position[i * (m_nbLegs - 1) + j] = m_legs[i].getJointValue(j);
        }
    }
}

/**
 * @brief Publish the current joints state of the robot
 *
 */
void QuadrupedRobot::publish()
{
    m_jointsState.header.stamp = m_node.get_clock()->now();
    m_publisher->publish(m_jointsState);

    // Publish foot position
    qr_interfaces::msg::FootPosition footPositionMsg;

    footPositionMsg.feet_1 = QR_Utils::eigenToVector3<double>(m_legs[0].getFeetPosition());
    footPositionMsg.feet_2 = QR_Utils::eigenToVector3<double>(m_legs[1].getFeetPosition());
    footPositionMsg.feet_3 = QR_Utils::eigenToVector3<double>(m_legs[2].getFeetPosition());
    footPositionMsg.feet_4 = QR_Utils::eigenToVector3<double>(m_legs[3].getFeetPosition());

    m_footPositionPublisher->publish(footPositionMsg);
}

/**
 * @brief Update and publish the joints state of the robot
 *
 */
void QuadrupedRobot::updateAndPublish()
{
    this->update();
    this->publish();
}

visualization_msgs::msg::Marker QuadrupedRobot::getMarker(const Eigen::Vector3d& p_pivotPosition)
{
    visualization_msgs::msg::Marker marker;

    marker.header.frame_id = "debug";

    marker.type = visualization_msgs::msg::Marker::CUBE;
    marker.action = visualization_msgs::msg::Marker::ADD;

    marker.scale.x = 0.0001;
    marker.scale.y = 0.0001;
    marker.scale.z = 100;

    marker.color.a = 0.3;

    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;

    marker.pose.position.x = p_pivotPosition(0);
    marker.pose.position.y = p_pivotPosition(1);
    marker.pose.position.z = p_pivotPosition(2);

    return marker;
}