#include "qr_kinematics/leg.hpp"
#include "qr_kinematics/quadruped_robot.hpp"
#include "qr_interfaces/msg/gait.hpp"
#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/twist.hpp>
#include <Eigen/Core>
#include <memory>

class Controller : public rclcpp::Node
{
public:
    Controller():
        Node("qr_controller"),
        m_linearVelocity(0.0),
        m_angularVelocity(0.0)
    {
        m_gaitSub = this->create_subscription<qr_interfaces::msg::Gait>("qr/gait_time", 10, std::bind(&Controller::gaitCallback, this, std::placeholders::_1));
        m_cmdTwistSub = this->create_subscription<geometry_msgs::msg::Twist>("qr/cmd_twist", 10, std::bind(&Controller::cmdTwistCallback, this, std::placeholders::_1));
    }

    ~Controller() {}

    void update()
    {
        m_robot.updateAndPublish();
    }

private:
    rclcpp::Subscription<qr_interfaces::msg::Gait>::SharedPtr m_gaitSub;
    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr m_cmdTwistSub;

    QuadrupedRobot m_robot;

    double m_linearVelocity;
    double m_angularVelocity;

private:
    void gaitCallback(const qr_interfaces::msg::Gait::SharedPtr p_msg)
    {
        m_robot.gait(p_msg, m_linearVelocity, m_angularVelocity);
    }

    void cmdTwistCallback(const geometry_msgs::msg::Twist::SharedPtr p_msg)
    {
        m_linearVelocity = p_msg->linear.x;
        m_angularVelocity = p_msg->angular.z;
    }
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<Controller> controller = std::make_shared<Controller>();

    while (rclcpp::ok())
    {
        controller->update();
        rclcpp::spin_some(controller);
    }

    rclcpp::shutdown();

    return 0;
}