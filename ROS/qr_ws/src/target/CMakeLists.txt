  cmake_minimum_required(VERSION 3.5)
project(target)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)

find_package(rclcpp REQUIRED)

find_package(geometry_msgs REQUIRED)
find_package(visualization_msgs REQUIRED)
find_package(interactive_markers REQUIRED)

find_package(tf2 REQUIRED)
find_package(tf2_ros REQUIRED)
find_package(tf2_geometry_msgs REQUIRED)

find_package(Eigen3 REQUIRED)

# Include Cpp "include" directory
include_directories(include)
include_directories(${EIGEN3_INCLUDE_DIR})

# Create Cpp executable
add_executable(spawn_target
  src/target.cpp
  src/spawn_target.cpp
)

ament_target_dependencies(spawn_target
  rclcpp
  geometry_msgs
  visualization_msgs
  interactive_markers
  tf2
  tf2_ros
  tf2_geometry_msgs
  Eigen3
)

# Install Cpp executables
install(
  TARGETS spawn_target
  DESTINATION lib/${PROJECT_NAME}
)

ament_package()