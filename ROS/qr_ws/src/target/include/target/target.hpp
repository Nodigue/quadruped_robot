#pragma once

#include <rclcpp/rclcpp.hpp>
#include <tf2_ros/transform_broadcaster.h>

#include <geometry_msgs/msg/pose.h>
#include <visualization_msgs/msg/marker.hpp>

#include <Eigen/Core>

#include <memory>
#include <string>

class Target : public rclcpp::Node
{
public:
	Target(const std::string& p_name, const std::string& p_frame);
	~Target() = default;

	inline Eigen::Matrix4d getTransform() const { return m_T; };

	void setTransform(const Eigen::Matrix4d& p_T);
	void setPose(const geometry_msgs::msg::Pose& p_pose);

	inline bool isReachable() const { return m_reachable; };
	void setReachability(const bool& p_reachable);

	void publishPose();

	visualization_msgs::msg::Marker getMarker();
	void publishMarker();

	void update();

private:

	Eigen::Matrix4d m_T;
	bool m_reachable;

	std::string m_frame;
	std::string m_topic;

	rclcpp::TimerBase::SharedPtr m_timer;
	std::unique_ptr<tf2_ros::TransformBroadcaster> m_tfBroadcaster;
	rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr m_markerPublisher;
};