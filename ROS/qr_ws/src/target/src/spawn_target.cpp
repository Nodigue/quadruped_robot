#include "target/target.hpp"

#include "rclcpp/rclcpp.hpp"
#include <tf2/LinearMath/Vector3.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include "visualization_msgs/msg/interactive_marker_control.hpp"
#include "visualization_msgs/msg/interactive_marker.hpp"
#include "interactive_markers/interactive_marker_server.hpp"

#include <memory>
#include <sstream>
#include <string>

visualization_msgs::msg::InteractiveMarker makeInteractiveMarker(const std::string& p_name, const std::string& p_frame, const tf2::Vector3& p_initialPosition)
{
    visualization_msgs::msg::InteractiveMarker interactiveMarker;

    interactiveMarker.header.frame_id = p_frame;

    interactiveMarker.name = p_name;
    interactiveMarker.description = "";
    interactiveMarker.scale = 0.1;

    interactiveMarker.pose.position.x = p_initialPosition.getX();
    interactiveMarker.pose.position.y = p_initialPosition.getY();
    interactiveMarker.pose.position.z = p_initialPosition.getZ();

    visualization_msgs::msg::InteractiveMarkerControl control;

    tf2::Quaternion orien(1.0, 0.0, 0.0, 1.0);
    orien.normalize();
    control.orientation = tf2::toMsg(orien);
    control.name = "rotate_x";
    control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::ROTATE_AXIS;
    interactiveMarker.controls.push_back(control);
    control.name = "move_x";
    control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::MOVE_AXIS;
    interactiveMarker.controls.push_back(control);

    orien = tf2::Quaternion(0.0, 1.0, 0.0, 1.0);
    orien.normalize();
    control.orientation = tf2::toMsg(orien);
    control.name = "rotate_z";
    control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::ROTATE_AXIS;
    interactiveMarker.controls.push_back(control);
    control.name = "move_z";
    control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::MOVE_AXIS;
    interactiveMarker.controls.push_back(control);

    orien = tf2::Quaternion(0.0, 0.0, 1.0, 1.0);
    orien.normalize();
    control.orientation = tf2::toMsg(orien);
    control.name = "rotate_y";
    control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::ROTATE_AXIS;
    interactiveMarker.controls.push_back(control);
    control.name = "move_y";
    control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::MOVE_AXIS;
    interactiveMarker.controls.push_back(control);

    return interactiveMarker;
}

void processFeedback(const visualization_msgs::msg::InteractiveMarkerFeedback::ConstSharedPtr& p_feedback, std::shared_ptr<Target>& p_targetPlane)
{
    p_targetPlane->setPose(p_feedback->pose);
}

int main(int argc, char ** argv)
{
    rclcpp::init(argc, argv);

    rclcpp::executors::SingleThreadedExecutor executor;

    std::shared_ptr<Target> targetPlane = std::make_shared<Target>("target", "base_link");
    executor.add_node(targetPlane);

    rclcpp::Node::SharedPtr interactiveTargetPlane = std::make_shared<rclcpp::Node>("InteractiveTarget");
    executor.add_node(interactiveTargetPlane);

    interactive_markers::InteractiveMarkerServer server("target", interactiveTargetPlane);
    visualization_msgs::msg::InteractiveMarker interactiveMarker = makeInteractiveMarker("target", "base_link", tf2::Vector3(0.0, 0.0, 0.0));

    server.insert(interactiveMarker, std::bind(&processFeedback, std::placeholders::_1, targetPlane));

    server.applyChanges();

    executor.spin();

    rclcpp::shutdown();

    return 0;
}
