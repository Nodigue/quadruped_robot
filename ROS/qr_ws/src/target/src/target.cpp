#include "target/target.hpp"

#include <geometry_msgs/msg/transform.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>

#include <Eigen/Geometry>

#include <chrono>

using namespace std::chrono_literals;

Target::Target(const std::string& p_name, const std::string& p_frame):
    Node(p_name),
    m_T(Eigen::Matrix4d::Identity()),
    m_reachable(false),
    m_frame(p_frame),
    m_topic(p_name)
{
    m_timer = this->create_wall_timer(100ms, std::bind(&Target::update, this));

    m_tfBroadcaster = std::make_unique<tf2_ros::TransformBroadcaster>(*this);
    m_markerPublisher = this->create_publisher<visualization_msgs::msg::Marker>(m_topic, 10);
}

/**
 * @brief Set the current pose of the target from a Eigen::Matrix4d.
 *
 * @param[in] p_T New pose of the target.
 */
void Target::setTransform(const Eigen::Matrix4d& p_T)
{
    m_T = p_T;
}

/**
 * @brief Set the current pose of the target from a geometry_msgs::msg::Pose.
 *
 * @param[in] p_pose New pose of the target.
 */
void Target::setPose(const geometry_msgs::msg::Pose& p_pose)
{
    // Position
    Eigen::Vector3d posV(p_pose.position.x, p_pose.position.y, p_pose.position.z);

    // Rotation
    Eigen::Quaterniond quat(p_pose.orientation.w, p_pose.orientation.x, p_pose.orientation.y, p_pose.orientation.z);
    Eigen::Matrix3d rotM = quat.matrix();

    m_T.block<3, 1>(0, 3) = posV;
    m_T.block<3, 3>(0, 0) = rotM;
}

/**
 * @brief Set the reachability of the target.
 *
 * @param[in] p_reachable New state of the target. Is it reachable or not ?
 */
void Target::setReachability(const bool& p_reachable)
{
    m_reachable = p_reachable;
}

/**
 * @brief Publish the pose (tf2) of the target.
 */
void Target::publishPose()
{
    geometry_msgs::msg::Transform tf;

    // Position
    tf.translation.x = m_T(0, 3);
    tf.translation.y = m_T(1, 3);
    tf.translation.z = m_T(2, 3);

    // Rotation
    Eigen::Matrix3d rotM = m_T.block<3, 3>(0, 0);
    Eigen::Quaterniond quat(rotM);

    tf.rotation.w = quat.w();
    tf.rotation.x = quat.x();
    tf.rotation.y = quat.y();
    tf.rotation.z = quat.z();

    geometry_msgs::msg::TransformStamped tfStamped;

    tfStamped.header.stamp = this->get_clock()->now();

    tfStamped.header.frame_id = m_frame;
    tfStamped.child_frame_id = m_topic;

    tfStamped.transform = tf;

    m_tfBroadcaster->sendTransform(tfStamped);
}

/**
 * @brief Contruct a visualization_msgs::msg::Marker to represent the target.
 * This function can be customized in order to represent the target type.
 *
 * @return visualization_msgs::msg::Marker Corresponding marker.
 */
visualization_msgs::msg::Marker Target::getMarker()
{
    visualization_msgs::msg::Marker marker;

    marker.header.frame_id = m_topic;

    marker.type = visualization_msgs::msg::Marker::CUBE;
    marker.action = visualization_msgs::msg::Marker::ADD;

    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;

    marker.color.a = 0.3;

    // If the target is reachable, the target marker appear green.
    if (m_reachable)
    {
        marker.color.r = 0.0;
        marker.color.g = 1.0;
        marker.color.b = 0.0;
    }

    // Otherwise the marker is red.
    else
    {
        marker.color.r = 1.0;
        marker.color.g = 0.0;
        marker.color.b = 0.0;
    }

    return marker;
}

/**
 * @brief Publish the visualization_msgs::msg::Marker of the target.
 */
void Target::publishMarker()
{
    visualization_msgs::msg::Marker marker = this->getMarker();
    m_markerPublisher->publish(marker);
}

/**
 * @brief Publish the pose and the marker of the target.
 */
void Target::update()
{
    this->publishPose();
    this->publishMarker();
}