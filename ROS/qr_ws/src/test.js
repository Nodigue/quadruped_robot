// contains the primary calculations of the sim
function wrun()
{
 // call run again in a moment
 if (wsimrun==true) wtimeout = setTimeout('wrun()', 30); // 30ms delay?
 
 // increment and loop wtime variable
 wtime += wdt;
 if (wtime > 1.0) wtime -= 1.0;

 // which leg grouping is down?
 sidedown = 0;
 if (wtime >= 0.5) sidedown = 1;

 // mywtime is a bad name
 // gives a [0,1] value for each half of the leg movement
 mywtime = wtime*2.0;
 if (wtime > 0.5) mywtime = (wtime-0.5)*2.0;
 
 // compute radius of curvature based on "curvature" from wslider
 if (Math.abs(curvature) < 0.0001) curvature = 0.0001;
 if (curvature > 0.0) cx = Math.tan((2.0-curvature)*3.1415/4.0)*50.;
 if (curvature < 0.0) cx = Math.tan((2.0-curvature)*3.1415/4.0)*50.;

 if (curvature < 0.0) sidedown = ((sidedown+1)%2);


 // length = radius * wangle
 // everyone needs same wangle, capped by leg with farthest length to step
 maxdist = 0.0;
 sweepwangle = 0.0;
 for (var ii=0; ii<6; ii++)
 {
  dist = Math.sqrt((cx-legx0[ii])*(cx-legx0[ii]) + legy0[ii]*legy0[ii]);
  if (dist > maxdist)
   maxdist = dist;
 }
 sweepwangle = Math.sign(cx)*25.0/maxdist;
 
 // center of body rotates and moves
 wang -= sweepwangle*wdt*2; // rotation is easy
 // shift body in direction of wangle
 wxpos -= sweepwangle*cx*wdt*2*Math.sin(wang);
 wypos -= sweepwangle*cx*wdt*2*Math.cos(wang);

 // compute where each leg is
 for (var ii=0; ii<6; ii++)
 {
  dist = Math.sqrt((cx-legx0[ii])*(cx-legx0[ii]) + legy0[ii]*legy0[ii]);
  tht0 = Math.atan2(legy0[ii], -(cx-legx0[ii]));
  wdtht = sweepwangle*Math.sign(cx);
  
  if (ii % 2 == sidedown)
  {
   // leg is down
   pos = -wdtht*(mywtime-0.5) + tht0;
   legx[ii] = cx+dist*Math.cos(pos);
   legy[ii] = dist*Math.sin(pos);
  } else {
   // leg is up
   pos = wdtht*(mywtime-0.5) + tht0;
   legx[ii] = cx+dist*Math.cos(pos);
   legy[ii] = dist*Math.sin(pos);
  }
 }

 wdraw();
}


function wdraw()
{ 
 wctx.fillStyle = 'white';
 wctx.fillRect(0, 0, 600, 200);

 // save position and wangle
 wctx.save();
 if (show_ground)
 {
 // draw background
 y0 = Math.round((wypos-200.0)/30.)*30. - wypos;
 x0 = Math.round((wxpos-200.0)/30.)*30. - wxpos;
 wctx.fillStyle = '#eeeeee';
 for (var ix=-10; ix<20; ix++)
 {
  for (var iy=-10; iy<20; iy++)
  {
   wctx.beginPath();
   x1 = ix*30 + x0;
   y1 = iy*30 + y0
   x = x1*Math.cos(wang) - y1*Math.sin(wang);
   y = x1*Math.sin(wang) + y1*Math.cos(wang);
   wctx.arc(x + 300, y + 80, 5, 0, 2.*Math.PI, false);
   wctx.fill();
  }
 }
 }

 if (show_turn_center)
 {
 // long leg path
 wctx.strokeStyle = '#eeeeee';
 wctx.lineWidth=2;
 for (var ii=0; ii<6; ii++)
 {
  dist = Math.sqrt((cx-legx0[ii])*(cx-legx0[ii]) + legy0[ii]*legy0[ii]);
  wctx.beginPath();
  wctx.arc(300+cx,80,dist, 0., 2.*Math.PI, false);
  wctx.stroke();
 }


 // small leg path
 wctx.strokeStyle = '#ccccff';
 wctx.lineWidth=4;
 for (var ii=0; ii<6; ii++)
 {
  dist = Math.sqrt((cx-legx0[ii])*(cx-legx0[ii]) + legy0[ii]*legy0[ii]);
  tht0 = Math.atan2(-legy0[ii], -(cx-legx0[ii]));
  wdtht = sweepwangle*0.5*Math.sign(cx);
  wctx.beginPath();
  wctx.arc(300+cx,80,dist, tht0-wdtht, tht0+wdtht, false);
  wctx.stroke();
 }
 }

 // draw border on top
 wctx.fillStyle = '#666666';
 wctx.fillRect(0, 0, 600, 3);
 wctx.fillRect(0, 0, 3, 200);
 wctx.fillRect(0, 197, 600, 3);
 wctx.fillRect(597, 0, 3, 200);
 wctx.fillRect(0, 160, 600, 3);
 wctx.fillStyle = '#ffffff';
 wctx.fillRect(3, 163, 594, 34);

 // draw helper circles
 wctx.lineWidth = 2;
 wctx.strokeStyle = '#fff8f8';

 // draw hex body as circle
 wctx.beginPath();
 wctx.arc(300,80,20, 0, 2 * Math.PI, false);
 wctx.lineWidth = 2;
 wctx.strokeStyle = '#999999';
 wctx.fillStyle = '#999999';
 wctx.stroke();
 wctx.fill();

 

 // draw each leg
 for (var ii=0; ii<6; ii++)
 {
  tht = 2.0*Math.PI*ii/6.;
  tht = legtht[ii];
  x0 = 300 + 20*Math.cos(tht);
  y0 = 80 + 20*Math.sin(tht);
  x1 = 300 + legx[ii];
  y1 = 80 + legy[ii];
  // foot-down dot
  if ((ii%2==sidedown&&cx>0) || (ii%2!=sidedown&&cx<0))
  {
   wctx.fillStyle = '#444444';
   wctx.beginPath();
   wctx.arc(x1,y1, 3, 0, 2 * Math.PI, false);
   wctx.fill();
  }
  wctx.strokeStyle = '#999999';
  wctx.lineWidth = 8;
  wctx.beginPath();
  wctx.moveTo(x0,y0);
  wctx.lineTo(x1,y1);
  wctx.stroke();
 }

 if (show_turn_center)
 {
 // draw circle center
 wctx.lineWidth = 1;
 wctx.fillStyle = '#ce8080';
 wctx.beginPath();
 wctx.arc(cx+300, 80, 5, 0, 2 * Math.PI, false);
 wctx.fill();
 }
  

  // wsliders
  wctx.strokeStyle = '#bbbbbb';
  wctx.lineWidth = 5;
 wctx.beginPath();
  wctx.moveTo(100,180);
  wctx.lineTo(500,180);
  wctx.stroke();
  wctx.lineWidth = 1;
  wctx.strokeStyle = '#dddddd';
  for (j=0; j<5; j++)
  {
 wctx.moveTo(100+j*400.0/4.0,170);
 wctx.lineTo(100+j*400.0/4.0,190);
 wctx.stroke();
  }
  
  // the slidey parts
  wctx.fillStyle = '#555555';
  wctx.fillRect(297+(curvature)*400/4+1,172,5,16);

  // display options (green = #83e171, red = #e1aa9d)
  wctx.strokeStyle = "#999999";
  wctx.lineWidth = 3;
  
 wctx.beginPath();
 wctx.arc(20,20, 10, 0, 2 * Math.PI, false);
 wctx.stroke();
 wctx.fillStyle = "#e1aa9d";
 if (show_ground) wctx.fillStyle = "#83e171";
 wctx.fill();
 wctx.fillStyle = "#999999";
 wctx.fillText("BACKGROUND",35,25);

 wctx.beginPath();
 wctx.arc(490,20, 10, 0, 2 * Math.PI, false);
 wctx.stroke();
 wctx.fillStyle = "#e1aa9d";
 if (show_turn_center) wctx.fillStyle = "#83e171";
 wctx.fill();
 wctx.fillStyle = "#999999";
 wctx.fillText("TURN GUIDES",505,25);

 wctx.fillStyle = "#777777";
 wctx.fillText("L",85,184);
 wctx.fillText("R",510,184);
}